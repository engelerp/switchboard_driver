import serial
import time

class Switchboard:
    def __init__(self, port, debug=False):
        """
        Constructor

        port is a serial port connected to the switchboard

        the serial port will be opened
        """
        self.ser = serial.Serial(port, 9600, timeout=5, bytesize=8, parity='N', stopbits=1)
        self.port = port
        self.allowed_channels = [2,3,4,5,6,7,8,9,10,11,12,13,14,15,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53]
        self.debug = debug
        self.current_channel = -1
        time.sleep(2) #ensure the reset on serial port connect finishes before any command can be issued

    def select_channel(self,channel):
        """
        Switch to specific channel; all other channels will be turned off

        channel is an integer

        returns True if switch was completed successfully
        returns False if the channel is invalid
        No side effects if channel is invalid
        """
        channel_int = int(channel)
        #weed out channels that will always be illegal
        if(not channel_int in self.allowed_channels):
            return False
        #place request to switchboard
        else:
            d1 = chr(int(channel_int/10.) + ord('0'))#extract 10-digit
            d0 = chr((channel_int % 10) + ord('0'))#extract 1-digit
            self.ser.write(b'c')
            self.ser.write(bytes(d1, encoding='utf8'))
            self.ser.write(bytes(d0, encoding='utf8'))
            response = self.ser.read(1)
            #response: success
            if response == b'A':
                if self.debug == True:
                    print(f"Sent: {c} {d1} {d0}")
                    print(f"Response: {response}")
                self.current_channel = channel
                return True
            #response: failure
            else:
                if self.debug == True:
                    print(f"Sent: {c} {d1} {d0}")
                    print(f"Response: {response}")
                return False

    def turn_off(self):
        """
        Turn off all channels

        returns True if operation was completed successfully
        returns False else
        No side effects if operation fails
        """
        self.ser.write(b'o')
        self.ser.write(b'f')
        self.ser.write(b'f')
        response = self.ser.read(1)
        #response: success
        if response == b'A':
            if self.debug == True:
                print(f"Sent: {o} {f} {f}")
                print(f"Response: {response}")
            self.current_channel = -1
            return True
        #response: failure
        else:
            if self.debug == True:
                print(f"Sent: {o} {f} {f}")
                print(f"Response: {response}")
            return False

    def close(self):
        """
        Close the serial port
        """
        self.ser.close()
